# The Complete 2020 Web Development Bootcamp

https://www.udemy.com/course/the-complete-web-development-bootcamp

Collection of all projects made in the bootcamp.

## HTML

• Personal website (html only)  
• Personal website (internal CSS)  
• Personal website (external CSS)

## CSS

• Stylised personal website

## Bootstrap

• TinDog

## jQuery

• Dice Game  
• Drum Kit  
• Simon Says Game

## Node - Express - APIs

• BMI calculator  
• WeatherApp  
• Newsletter Signup

## EJS

• Blog